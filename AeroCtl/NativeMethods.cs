﻿// ReSharper disable InconsistentNaming

namespace Windows.Win32;

public static partial class PInvoke
{
	// Missing from CsWin32
	public const int HIDP_STATUS_SUCCESS = 0x11 << 16;
}
