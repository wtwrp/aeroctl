﻿namespace AeroCtl.UI;

public enum UpdateMode
{
	Light,
	Normal,
	Full,
}